{
  open Parser
  exception Eof
}

let aeiou = ['a' 'e' 'i' 'o' 'u']
let aeou = ['a' 'e' 'o' 'u']
let aei = ['a' 'e' 'i']
let u_AEIOU = ['A' 'E' 'I' 'O' 'U']
let klmnps = ['k' 'l' 'm' 'n' 'p' 's']
let klps = ['k' 'l' 'p' 's']
let u_KLMNPS = ['K' 'L' 'M' 'N' 'P' 'S']
let jt = ['j' 't']
let u_JT = ['J' 'T']
let n = ['n']
let w = ['w']
let u_W = ['W']

rule token = parse
  [' ' '\t' ',']	{ token lexbuf }
| ['\n' '\r'] { Lexing.new_line lexbuf ; token lexbuf }
| "pi" { PI }
| "li" { LI }
| "la" { LA }
| "e" { E }
| "o" { O }
| "mi" { MI }
| "sina" { SINA }
| "ala" { ALA }
| "lon" { LON }
| "kepeken" { KEPEKEN }
| "tan" { TAN }
| "tawa" { TAWA }
| "sama" { SAMA }
| "en" { EN }
| "anu" { ANU }
| ['.' ':' '!' '?'] { PERIOD }
| (klmnps aeiou|aeiou|jt aeou|w aei)(n klps aeiou|klmnps aeiou|n? jt aeou|n? w aei)*n? as lxm { WORD(lxm) }
| (u_KLMNPS aeiou|u_AEIOU|u_JT aeou|u_W aei)(n klps aeiou|klmnps aeiou|n? jt aeou|n? w aei)*n? as lxm { LOAN(lxm) }
| ['A'-'Z''a'-'z''_']+ as lxm { Printf.printf "PAKALA: nimi \"%s\" li ike tawa toki pona.\n" lxm; failwith "Bad input" }
| eof { PERIOD }
| _ as lxm { Printf.printf "Illegal character %c\n" lxm; failwith "Bad input" }
