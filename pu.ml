open Lexer ;;
open Parser ;;

let parse_file name =
  let chan = open_in name in
  let lexbuf = Lexing.from_channel chan in
  let p = Parser.start Lexer.token lexbuf in
    close_in chan;
    p

let main () =
  try
    let p = parse_file Sys.argv.(1) in
    Printf.printf "ni li pu!\n"
  with _ -> Printf.printf "ni li pu ala.\n"
;;

main ()
