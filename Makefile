.PHONY = all,clean

all:
	rm -f *.native pu.byte
	ocamlbuild -cflag -g -yaccflag -v -lflag -g pu.byte

clean:
	rm -f *.native pu.byte
