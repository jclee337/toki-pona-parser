%{
  open Lexing ;;

  exception ParseErr of string

  let error msg start finish  =
    Printf.printf "PAKALA (linja %d: nimi %d..%d): %s" start.pos_lnum
        (start.pos_cnum -start.pos_bol) (finish.pos_cnum - finish.pos_bol) (msg ^ "\n")

  let parse_err msg nterm = error msg (rhs_start_pos nterm) (rhs_end_pos nterm)
%}

%token <string> WORD
%token <string> LOAN
%token LI LA PI E O MI SINA ALA PERIOD
%token LON KEPEKEN TAWA TAN SAMA
%token EN ANU
%token EOF
%token ERR
%start start
%type <bool> start
%%

start
  : sentences PERIOD { true }
  ;

sentences
  : { true }
  | sentence PERIOD sentences { true }
  ;

sentence
  : sentence LA sentence { true }
  | phrase LA sentence { true }
  | O phrase e_list li_list { true }
  | MI phrase e_list li_list { true }
  | MI O phrase e_list li_list { true }
  | SINA phrase e_list li_list { true }
  | SINA O phrase e_list li_list { true }
  | subject li_list { true }
  ;

li_list
  : { true }
  | LI phrase e_list li_list { true }
  | O phrase e_list li_list { true }
  ;

e_list
  : { true }
  | E phrase prep_list e_list { true }
  ;

prep_list
  : { true }
  | LON phrase prep_list { true }
  | KEPEKEN phrase prep_list { true }
  | TAWA phrase prep_list { true }
  | TAN phrase prep_list { true }
  | SAMA phrase prep_list { true }
  ;

subject
  : phrase compound_s { true }
  | no_mi_sina word_list pi_list { true }
  ;

compound_s
  : EN phrase compound { true }
  | ANU phrase compound { true }
  ;

compound
  : { true }
  | EN phrase compound { true }
  | ANU phrase compound { true }
  ;

phrase
  : word word_list pi_list { true }
  ;

word_list
  : { true }
  | word word_list { true }
  | LOAN word_list { true }
  ;

word
  : MI { true }
  | SINA { true }
  | no_mi_sina { true }
  ;

no_mi_sina
  : WORD { true }
  | LON { true }
  | KEPEKEN { true }
  | TAWA { true }
  | TAN { true }
  | SAMA { true }
  | ALA { true }
  ;

pi_list
  : { true }
  | PI phrase pi_list { true }
  ;
